//
//  Fraction+MathOps.h
//  objc-programming
//
//  Created by Daniel Delgado on 10/5/20.
//  Copyright © 2020 Daniel Delgado. All rights reserved.
//

#import "Fraction.h"

NS_ASSUME_NONNULL_BEGIN

@interface Fraction (MathOps)

-(Fraction *) add: (Fraction *) f;
-(Fraction *) mul: (Fraction *) f;
-(Fraction *) sub: (Fraction *) f;
-(Fraction *) div: (Fraction *) f;

@end

NS_ASSUME_NONNULL_END
