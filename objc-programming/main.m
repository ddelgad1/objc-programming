//
//  main.m
//  objc-programming
//
//  Created by Daniel Delgado on 8/30/20.
//  Copyright © 2020 Daniel Delgado. All rights reserved.
//

/**
 Fraction test
 */
#import <Foundation/Foundation.h>
#import "Fraction.h"
#import "Calculator.h"
#import "Complex.h"
#import "Fraction+MathOps.h"

#define CHAPTER 2

// function prototypes
void testPrimitives(void);
void arithmeticOperators(void);
void integerArithmetic(void);
void implicitConversion(void);
void testCalculator(void);
void printObject(id obj);

int main(int argc, const char * argv[])
{
    @autoreleasepool {
        Fraction *a = [[Fraction alloc] init];
        Fraction *b = [[Fraction alloc] init];
        Fraction *result;

        [a setTo: 1 over: 3];
        [b setTo: 2 over: 5];

        [a print]; NSLog (@"  +"); [b print]; NSLog (@"-----");
        result = [a add: b];
        [result print];
        NSLog (@"\n");

        [a print]; NSLog (@"  -"); [b print]; NSLog (@"-----");
        result = [a sub: b];
        [result print];
        NSLog (@"\n");

        [a print]; NSLog (@"  *"); [b print]; NSLog (@"-----");
        result = [a mul: b];
        [result print];
        NSLog (@"\n");

        [a print]; NSLog (@"  /"); [b print]; NSLog (@"-----");
        result = [a div: b];
        [result print];
        NSLog (@"\n");
//        enum month { january = 1, february, march, april, may, june,
//                         july, august, september, october, november,
//                         december };
//        
//              enum month amonth;
//        
//              int     days;
//
//              NSLog (@"Enter month number: ");
//              scanf ("%i", &amonth);
//
//              switch (amonth) {
//                 case january:
//                 case march:
//                 case may:
//                 case july:
//                 case august:
//                 case october:
//                 case december:
//                          days = 31;
//                          break;
//                 case april:
//                 case june:
//                 case september:
//                 case november:
//                          days = 30;
//                          break;
//                 case february:
//                          days = 28;
//                          break;
//                 default:
//                          NSLog (@"bad month number");
//                          days = 0;
//                          break;
//              }
//
//              if ( days != 0 )
//                 NSLog (@"Number of days is %i", days);
//
//              if ( amonth == february )
//                 NSLog (@"...or 29 if it's a leap year");
//        Fraction *a, *b, *c;
//
//        NSLog (@"Fractions allocated: %i", [Fraction count]);
//
//        a = [[Fraction allocF] init];
//        b = [[Fraction allocF] init];
//        c = [[Fraction allocF] init];
//
//        NSLog (@"Fractions allocated: %i", [Fraction count]);
//        Fraction *a, *b, *c, *d;
//        a = [[Fraction alloc] initWith: 1 over: 3];
//        b = [[Fraction alloc] initWith: 3 over: 7];
//        c = [[Fraction alloc] initWith:2 over:3];
//        d = [[Fraction alloc] init];
//
//        [a print];
//        [b print];
//        [c print];
//        [d print];
        
    //NSLog(@"x: %i", gSuperX);
//        id dataValue;
//        Fraction *f1 = [[Fraction alloc] init];
//        Complex  *c1 = [[Complex alloc] init];
//        Calculator *cal = [[Calculator alloc] init];
//
//        [f1 setTo: 2 over: 5];
//        [c1 setReal: 10.0 andImaginary: 2.5];
//        [cal setAccumulator:5];
        
        // first dataValue gets a fraction
        
//        dataValue = f1;
//        [dataValue print];
//
//        // now dataValue gets a complex number
//
//        dataValue = c1;
//        [dataValue print];
        
//        printObject(f1);
//        printObject(c1);
        //printObject(cal);
    }
    return 0;
} // end of main

void printObject(id obj) {
//    // if the object can print
//    if ([obj respondsToSelector: @selector(print)]){
//        [obj print];
//    } else {
//        // else display messages
//        NSLog(@"Object does not have a print method");
//    }
//    [obj print];
    @try {
        [obj print];
    } @catch (NSException *exception) {
        NSLog(@"Object does not have a print method. %@", exception);
    } @finally {
        NSLog(@"This always runs)");
    }
    NSLog(@"No errors.");
}

// functions implementation
void testPrimitives(void) {
    int integerVar = 100;
    float floatingVar = 331.79;
    double doubleVar = 8.44e+11;
    char charVar = 'W';
    
    NSLog (@"integerVar = %i", integerVar);
    NSLog (@"floatingVar = %f", floatingVar);
    NSLog (@"doubleVar = %e", doubleVar);
    NSLog (@"doubleVar = %g", doubleVar);
    NSLog (@"charVar = %c", charVar);
    
}

void arithmeticOperators(void) {
    int a = 100;
    int b = 2;
    int c = 25;
    int d = 4;
    int result;
    
    result = a - b;
    
    // subtraction
    NSLog (@"a - b = %i", result);
    
    result = b * c;
    
    // multiplication
    NSLog (@"b * c = %i", result);
    
    result = a / c;
    
    // division
    NSLog (@"a / c = %i", result);
    
    result = a + b * c;
    // precedence
    NSLog (@"a + b * c = %i", result);
    NSLog (@"a * b + c * d = %i", a * b + c * d);
}

void integerArithmetic(void) {
    int a = 25;
    int b = 2;
    float c = 25.0;
    float d = 2.0;
    
    NSLog (@"6 + a / 5 * b = %i", 6 + a / 5 * b);
    NSLog (@"a / b * b = %i", a / b * b);
    NSLog (@"c / d * d = %f", c / d * d);
    NSLog (@"-a = %i", -a);
}

void implicitConversion(void) {
    float f1 = 123.125, f2;
    int i1, i2 = -150;
    
    i1 = f1;
    
    // floating to integer conversion
    NSLog (@"%f assigned to an int produces %i", f1, i1); f1 = i2;
    
    // integer to floating conversion
    NSLog (@"%i assigned to a float produces %f", i2, f1); f1 = i2 / 100;
    
    // integer divided by integer
    NSLog (@"%i divided by 100 produces %f", i2, f1); f2 = i2 / 100.0;
    
    // integer divided by a float
    NSLog (@"%i divided by 100.0 produces %f", i2, f2); f2 = (float) i2 / 100;
    
    // type cast operator
    NSLog (@"(float) %i divided by 100 produces %f", i2, f2);
}

void testCalculator(void) {
    
    //    Calculator *deskCalc;
    //    deskCalc = [Calculator alloc];
    //    deskCalc = [deskCalc init];
    Calculator *deskCalc = [[Calculator alloc] init];
    //
    //    [deskCalc setAccumulator: 100.0];
    //    [deskCalc add: 200.];
    //    [deskCalc divide: 15.0];
    //    [deskCalc subtract: 10.0];
    //    [deskCalc multiply: 5];
    
    double  value1, value2;
    char    operator;
    
    //NSLog (@"The result is %g", [deskCalc accumulator]);
    
    NSLog (@"Type in your expression.");
    
    scanf ("%lf %c %lf", &value1, &operator, &value2);
    
    [deskCalc setAccumulator: value1];
    
    switch ( operator )
    { case '+':
            [deskCalc add: value2];
            break;
        case '-':
            [deskCalc subtract: value2];
            break;
        case '*':
            [deskCalc multiply: value2];
            break;
        case '/':
            [deskCalc divide: value2];
            break;
        default: NSLog (@"Unknown operator.");
            break;
            
    }
    
    NSLog (@"%.2f", [deskCalc accumulator]);
}
