//
//  Fraction.h
//  objc-programming
//
//  Created by Daniel Delgado on 8/30/20.
//  Copyright © 2020 Daniel Delgado. All rights reserved.
//

// interface seccion -----

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface Fraction : NSObject

@property int numerator;
@property int denominator;

-(void) print; // instance method

-(double) convertToNum;

-(void) setTo: (int) n over: (int) d;

-(void) reduce;
- (instancetype) initWith: (int) n over: (int) d;

+(Fraction *) allocF;
+(int) count;

@end

NS_ASSUME_NONNULL_END
