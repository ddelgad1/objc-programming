// Interface file for Complex class

#import <Foundation/Foundation.h>

@interface Complex: NSObject

// 34 + 2i --> square root of -1
@property double real, imaginary;

-(void)   print;
-(void)   setReal: (double) a andImaginary: (double) b;
-(Complex *) add: (Complex *) f;

@end
