//
//  Calculator.h
//  objc-programming
//
//  Created by Daniel Delgado on 9/6/20.
//  Copyright © 2020 Daniel Delgado. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface Calculator : NSObject

// accumulator methods
-(void) setAccumulator: (double) value;
-(void) clear;
-(double) accumulator;

// arithmetic methods
-(void) add: (double) value;
-(void) subtract: (double) value;
-(void) multiply: (double) value;
-(void) divide: (double) value;

@end

NS_ASSUME_NONNULL_END
