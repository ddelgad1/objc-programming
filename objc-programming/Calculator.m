//
//  Calculator.m
//  objc-programming
//
//  Created by Daniel Delgado on 9/6/20.
//  Copyright © 2020 Daniel Delgado. All rights reserved.
//

#import "Calculator.h"

@implementation Calculator
{
    double accumulator; // instance variables
    
}

-(void) setAccumulator: (double) value
{
    accumulator = value;
}

-(void) clear {
    
    accumulator = 0;
    
}

-(double) accumulator
{
    return accumulator;
}

-(void) add: (double) value
{
    accumulator += value;
    //accumulator = accumulator + value;
}

-(void) subtract: (double) value
{
    accumulator -= value;
}

-(void) multiply: (double) value
{
    accumulator *= value;
    
}

-(void) divide: (double) value
{
    accumulator /= value;
}

@end
