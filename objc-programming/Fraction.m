//
//  Fraction.m
//  objc-programming
//
//  Created by Daniel Delgado on 8/30/20.
//  Copyright © 2020 Daniel Delgado. All rights reserved.
//

// implementation seccion ----

#import "Fraction.h"

static int gCounter;

@implementation Fraction


@synthesize numerator, denominator;

-(void) print
{
    NSLog(@"%i/%i", numerator, denominator);
}

- (double) convertToNum
{
    if (denominator != 0)
    {
        return (double) numerator / denominator;
    }
    else
    {
        return NAN;
    }
}

- (void)setTo:(int) n over:(int) d {
    numerator = n;
    denominator = d;
}

// add a fraction to the receiver
- (Fraction *) add: (Fraction *) f {
    
    Fraction *result = [[Fraction alloc] init];
    
    // To add two fractions: // a/b + c/d = ((a*d) + (b*c)) / (b * d)
    result.numerator = self.numerator * [f denominator] + self.denominator * [f numerator];
    
    result.denominator = self.denominator * [f denominator];
    
    [result reduce];
    
    return result;
    
}

- (void) reduce
{
   // local variables
   int  u = numerator;
   int  v = denominator;
   int  temp;

   while (v != 0) {
      temp = u % v;
    u = v;
      v = temp;
   }

   numerator /= u;
   denominator /= u;
    
}

-(instancetype)  init
{
    return  [self  initWith: 1 over: 1];
}

- (instancetype) initWith: (int) n over: (int) d
{
    self = [super init];
    if (self) {
        [self setTo:n over:d];
    }
    return self;
}

+(Fraction *) allocF {
    extern int gCounter;
    ++gCounter;
    return [Fraction alloc];
    
}

+(int) count {
    extern int gCounter;
    return gCounter;
}

@end
